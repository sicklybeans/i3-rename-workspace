import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,Gdk,GdkPixbuf,GObject

class DialogExample(Gtk.Dialog):

  def __init__(self, parent, default_text='default'):
    Gtk.Dialog.__init__(
      self, 'Export playlist', parent, 0,
      (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK))
    self.set_default_size(150, 100)

    label = Gtk.Label("This is a dialog to display additional information")

    entry = Gtk.Entry()
    entry.set_text(default_text)
    entry.connect('key-press-event', self._on_key_press_event)

    box = self.get_content_area()
    box.add(label)
    box.add(entry)

    # self.add_action_widget(entry, 97)

#    self.run()
    self.show_all()

  def _on_okay(self):
    print('okay')
    self.emit('response', 97)

  def _on_cancel(self):
    print('cancel')

  def _on_key_press_event(self, _, event : Gdk.EventKey):
    print('key press: %s' % str(event.get_keyval()))
    if event.keyval == Gdk.KEY_Return:
      self._on_okay()
    else:
      print('no match')

if __name__ == '__main__':
  de = DialogExample(None)
  print(de.run())
  #Gtk.main()