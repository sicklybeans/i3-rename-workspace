#include <gtk/gtk.h>
#include <stdio.h>

const static char* DESCRIPTION = "Versatile program that displays dialog prompting user for input string then prints response to standard out.";

/**
 * Struct containing the name and description of each program argument.
 */
struct argdes {
  const char* name;
  const char* description;
};

const static int ARGUMENT_NUMBER = 3;
const static struct argdes ARGUMENTS[] = {
  {"title", "Title of dialog window"},
  {"prompt", "Instructional text that accompanies input prompt"},
  {"default", "Default text to fill the input prompt with"}
};

static int on_key_press(GtkWidget* entry, GdkEventKey* key, GtkDialog* dialog) {
  if (key->keyval == GDK_KEY_Return) {
    gtk_dialog_response(dialog, 0);
    return TRUE; // stop signal propagation.
  }
  return FALSE; // continue signal propagation.
}

char* run_dialog(const char* dialog_title, const char* prompt_text, const char* default_text) {
  GtkWidget *dialog, *label, *content_area, *entry;
  GtkDialogFlags flags = GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT;
  int status;

  dialog = gtk_dialog_new_with_buttons(dialog_title, NULL, flags, NULL);

  gtk_dialog_add_button((GtkDialog*) dialog, "_Ok", GTK_RESPONSE_ACCEPT);
  gtk_dialog_add_button((GtkDialog*) dialog, "Cancel", GTK_RESPONSE_CANCEL);

  content_area = gtk_dialog_get_content_area((GtkDialog*) dialog);

  label = gtk_label_new(prompt_text);
  gtk_container_add((GtkContainer*) content_area, label);

  entry = gtk_entry_new();
  gtk_entry_set_text((GtkEntry*) entry, default_text);
  gtk_container_add((GtkContainer*) content_area, entry);
  g_signal_connect(entry, "key-press-event", G_CALLBACK (on_key_press), dialog);

  gtk_widget_show_all(dialog);
  status = gtk_dialog_run((GtkDialog*) dialog);

  if (status == GTK_RESPONSE_CANCEL)
    return NULL;
  else {
    int length = strlen(gtk_entry_get_text((GtkEntry*) entry));
    char* str = (char*) malloc(sizeof(char)*(length+1));
    strcpy(str, gtk_entry_get_text((GtkEntry*) entry));
    return str;
  }
}

void print_usage(FILE* stream, const char* progname) {
  if (stream == NULL)
    stream = stdout;
  fprintf(stream, "Usage: %s [-h] ", progname);
  for (int i=0; i<ARGUMENT_NUMBER; i++) {
    fprintf(stream, "%s ", ARGUMENTS[i].name);
  }
  fprintf(stream, "\n");
}

void print_help(FILE* stream, const char* progname) {
  print_usage(stream, progname);
  fprintf(stream, "\n%s\n\n", DESCRIPTION);
  fprintf(stream, "Arguments:\n");
  for (int i=0; i<ARGUMENT_NUMBER; i++) {
    fprintf(stream, "  %-10s  %s\n", ARGUMENTS[i].name, ARGUMENTS[i].description);
  }
}

void bad_usage(const char* progname, const char* reason) {
  fprintf(stderr, "Bad usage: %s\n", reason);
  print_usage(stderr, progname);
  exit(1);
}

int main (int argc, char **argv) {
  gtk_init (&argc, &argv);

  if (argc > 1 && strcmp(argv[1], "-h") == 0) {
    print_help(stdout, argv[0]);
    exit(0);
  }

  if (argc != 4) {
    bad_usage(argv[0], "Wrong number of arguments");
  }

  char* result = run_dialog(argv[1], argv[2], argv[3]);

  if (result == NULL) {
    exit(1);
  } else {
    printf("%s\n", result);
    exit(0);
  }
}