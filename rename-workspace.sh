#!/bin/bash
# This is an i3 companion script that renames the current workspace by showing a
# dialog with a prompt for a new name.
# It requires the program "sjb-dialog-prompt"

# First we want the name/number
CURRENT_WS=$(i3-msg -t get_workspaces | sed -E 's/"rect":\{[^\}]*\},//g' | sed -E 's/\},\{/\n/g' | grep "\"focused\":true")
NAME=$(echo "$CURRENT_WS" | sed -E 's/.*"name":"([^"]*)".*/\1/g')
NUM=$(echo "$CURRENT_WS" | sed -E 's/.*"num":([^,]*),.*/\1/g')

# Prompt the user for a new name
NEW_NAME=$(sjb-dialog-prompt "Rename current workspace" "New workspace name for workspace '$NAME' (# $NUM)" "$NAME")
if [[ "$?" -ne 0 ]]; then
	echo "Canceled" > /dev/stderr
	exit 1
else
	i3-msg 'rename workspace "'"$NAME"'" to "'"$NEW_NAME"'"'
	exit "$?"
fi
